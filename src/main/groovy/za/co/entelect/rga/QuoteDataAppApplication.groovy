package za.co.entelect.rga

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class QuoteDataAppApplication {

	static void main(String[] args) {
		SpringApplication.run(QuoteDataAppApplication, args)
	}

}
